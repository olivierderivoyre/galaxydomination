package com.olivier1664.GalaxyDomination;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Artificial Intelligence.
 * 
 * Ok, it is just some greedy heuristics, but come on, how do not love thinking computer is some king of stupid guy? 
 */
public interface AI {

	public void Play(GalaxyThread galaxy, Player me, Planet[] myPlanets,
			Planet[] otherPlanets,
			HashMap<Planet, ArrayList<Planet>> accessiblePlanets);

}
