﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 30; i++)
            {
                Button button = new Button();
                button.BackColor = Color.LightGray;
                button.Text = "0";
                button.Top = 10;
                button.Left = 400;
                button.Width = button.Height = 36;
                button.Click += button_Click;
                button.MouseMove += button_MouseMove;
                button.MouseUp += button_MouseUp;
                button.Tag = 1;
                this.Controls.Add(button);
            }
            for (int i = 0; i < 30; i++)
            {
                Button button = new Button();
                button.BackColor = Color.LightGray;
                button.Text = "10";
                button.Top = 100;
                button.Left = 400;
                button.Width = button.Height = 50;
                button.Click += button_Click;
                button.MouseMove += button_MouseMove;
                button.MouseUp += button_MouseUp;
                button.Tag = 2;
                this.Controls.Add(button);
            }
            for (int i = 0; i < 30; i++)
            {
                Button button = new Button();
                button.BackColor = Color.LightGray;
                button.Text = "20";
                button.Top = 200;
                button.Left = 400;
                button.Width = button.Height = 90;
                button.Click += button_Click;
                button.MouseMove += button_MouseMove;
                button.MouseUp += button_MouseUp;
                button.Tag = 3;
                this.Controls.Add(button);
            }

        }

       



        private void button_Click(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button) sender;
                int n = int.Parse(b.Text);
                n = (n + 5);
                if (n == 35)
                {
                    n = 50;
                }
                if (n > 999)
                {
                    n = 0;
                } else if (n == 55)
                {
                    n = 999;
                }
                b.Text = n.ToString();
                this.showOutput();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);                
            }
        }

        private void button_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (e.Button == MouseButtons.Right)
                {
                    Trace.WriteLine(e.X + " x " + e.Y);
                    Trace.WriteLine("c1:  " + Cursor.Position.X + " x " + Cursor.Position.Y);
                    Point clientPoint =  this.PointToClient(Cursor.Position);
                    Trace.WriteLine("c2:  " + clientPoint.X + " x " + clientPoint.Y);

                    b.Left = clientPoint.X - b.Width / 2;
                    b.Top = clientPoint.Y - b.Height / 2;
                }
                else
                {
                    showOutput();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
        private void button_MouseUp(object sender, MouseEventArgs e)
        {

            try
            {
                Button b = (Button)sender;
                if (e.Button == MouseButtons.Middle)
                {
                    if (b.Font.Bold)
                    {
                        b.Font = Control.DefaultFont;
                    }
                    else
                    {
                        b.Font = new Font(Control.DefaultFont, FontStyle.Bold);
                    }
                }
                else
                {
                    showOutput();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
            }
        }
        private void showOutput()
        {
            StringBuilder output = new StringBuilder { };
            int playerStart = 0;
             foreach (Button c in this.getPlanets())
            {                             
                int x = c.Left + c.Width / 2;               
                int y = 400 - (c.Top + c.Height / 2);
                int radius = (int)c.Tag;
                int pop = int.Parse(c.Text);
                int playerId = 0;
                if (c.Font.Bold)
                {
                    playerStart++;
                    playerId = playerStart;
                }
                output.AppendLine("\t\t\t" + x + ", " + y + ", " + radius + ", " + pop + ", " + playerId + ",");
            }
            output.AppendLine("\t\t};");
            this.richTextBox.Text = "\t\tint[] data = {" + playerStart + ",\n" + output.ToString();
        }

        private List<Button> getPlanets()
        {
            List<Button> returned = new List<Button>();
            foreach (Control c in this.Controls)
            {
                if (!(c is Button))
                {
                    continue;
                }
                int x = c.Left + c.Width / 2;
                if (x > 300)
                {
                    continue;
                }
                returned.Add((Button)c);
            }
            returned.Sort(compareButton);
            return returned;
        }
        private int compareButton(Button b1, Button b2)
        {
            if (b1.Top != b2.Top)
            {
                return b2.Top.CompareTo(b1.Top);
            }
            return b1.Left.CompareTo(b2.Left);
        }
    }
}

